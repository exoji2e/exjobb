import java.util.*;
import java.io.*;
public class TestRangeTree {
    Random r = new Random(37);
    public void run(){
        test(100, 1);
        test(100, 2);
        test(100, 3);
        test(1000, 3);
        test(1000, 3);
        test(1000, 3);
    }
    public static void main(String[] args) throws Exception{
        TestRangeTree trt = new TestRangeTree();
        trt.run();
    }
    int ri() {
        return r.nextInt(1000);
    }
    Point rp(int i, int d) {
        Point p = new Point();
        p.id = i;
        p.v = ri();
        p.vs = new int[d];
        for(int j = 0; j<d; j++) {
            p.vs[j] = ri();
        }
        return p;
    }
    boolean test(int n, int d) {
        Point[] pts = new Point[n];
        for(int i = 0; i<n; i++) {
            pts[i] = rp(i, d);
        }
        
        RangeTree rt = RangeTree.make_range_tree(pts, 0, true);
        StringBuilder sb = new StringBuilder();
        for(Point p: pts) {
            sb.append(p.v).append(' ');
            for(int i = 0; i<d; i++)
                sb.append(p.vs[i]).append(' ');
            sb.append('\n');
        }
        rt.log(sb);
        Point l = rp(0, d);
        Point r = rp(0, d);
        sb.append("------------L---------\n");
        for(int i = 0; i<d; i++) {
            sb.append(l.vs[i] + " ");
        }
        sb.append("\n------------R---------\n");
        for(int i = 0; i<d; i++) {
            sb.append(r.vs[i] + " ");
        }
        sb.append('\n');
        for(int i = 0; i<d; i++) {
            int min = Math.min(l.vs[i], r.vs[i]);
            int max = Math.max(l.vs[i], r.vs[i]);
            l.vs[i] = min;
            r.vs[i] = max;
        }
        int[] m1 = rt.max(l, r);
        int[] m2 = max(pts, l, r);
        System.out.println(m1[1] + " " + m1[0] + " " + m2[0] + " " + m2[1]);
        if(m1[0] != m2[0]) {
            if (m1[1] != -1) {
                Point p = pts[m1[1]];
                for(int i = 0; i<d; i++) 
                    System.out.println(l.vs[i] + " " + p.vs[i] + " " + r.vs[i]);
            }
            if(m2[1] != -1) {
                Point p = pts[m2[1]];
                for(int i = 0; i<d; i++) 
                    System.out.println(l.vs[i] + " " + p.vs[i] + " " + r.vs[i]);

            }
        }
        return m1[0] == m2[0];
    }
    int[] max(Point[] pts, Point l, Point r) {
        int max = Integer.MIN_VALUE;
        int id = -1;
        for(Point p: pts) {
            boolean ok = true;
            for(int i = 0; i<l.vs.length; i++) {
                if(p.vs[i] < l.vs[i]) ok = false;
                if(p.vs[i] > r.vs[i]) ok = false;
            }
            if(ok && max < p.v) {
                max = p.v;
                id = p.id;
            } 
        }
        return new int[]{max, id};
    }
}
