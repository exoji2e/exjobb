import java.util.*;
import java.io.*;
public class Util {
    static int toInt(String s) {return Integer.parseInt(s);}
    static int[] toInts(String s) {
        String[] a = s.split(" ");
        int[] o = new int[a.length];
        for(int i = 0; i<a.length; i++) o[i] = toInt(a[i]);
        return o;
    }
    static void fail() {
        throw new IllegalArgumentException();
    }
    static void e(Object o) {
        System.err.println(o);
    }
    static HashSet<Integer> copy(HashSet<Integer> set) {
        return (HashSet<Integer>) set.clone();
    }
    static String nxt(BufferedReader br) {
        try {
            String line = br.readLine();
            while(line.charAt(0) == 'C' || line.charAt(0) == 'c') line = br.readLine();
            return line;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }

    }
}
