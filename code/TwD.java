import java.util.*;
import java.io.*;
public class TwD {
    void solve(String[] args) throws Exception {
        boolean oneIndex = args.length > 1;
        String grpath = args[0];
        // Parse data
        Graph G = Graph.build(grpath, oneIndex);

        String tdpath = grpath.substring(0, grpath.length() - 3) + ".td";
        TreeDecomposition T = TreeDecomposition.build(tdpath, oneIndex);
        Util.e(String.format("%d bags in td", T.bags.length));

        System.out.println(A(G, T));

    }
    public int A(Graph G, TreeDecomposition T) {
        return A(G, T, 0);
    }
    public int A(Graph G, TreeDecomposition T, int maxbe4) {
        int n = G.nodes.length, k=T.k;
        if(n < maxbe4) return 0;
        //TODO: if k < log n: call diameter on G.
        if(k > 30 || (1<<k) > n || T.bags.length < 3){
            return G.diameter_dijk();
        }
        if(k > 10){
            System.out.println(n);
            System.out.println(k);
            throw new IllegalArgumentException(); 
        }
        // Find centroid C of T
        Bag C = T.centroid(n/2);
        int cs = C.nodes.size();
        int[][] dists = new int[cs][n];
        int tmp = 0;
        Node[] Cn = new Node[cs];
        // dijkstra from each node in C
        for(Integer nod: C.nodes) {
            Node node = G.nodes[nod];
            G.dijkstra(node, dists[tmp]);
            Cn[tmp++] = node;
        }
        tmp = 0;
        for(Integer c_i: C.nodes) {
            for(Integer c_j: C.nodes) {
                if(c_j != c_i) {
                    G.upd_edge(c_i, c_j, dists[tmp][c_j]);
                }
            }
            tmp++;
        }

        //Not the diameter, just making sure we calculate the canidates right.

        int max = 0;
        for(int[] v : dists){ 
            for(int d: v) {
                max = Math.max(max, d);
            }
        }
        if(max*2 <= maxbe4) return 0;
        Util.e(String.format("Max dijkstra dist %d", max));
        Util.e(String.format("GraphSize %d", n));

        Bag TA = null;
        HashSet<Integer> A = new HashSet<>();
        for(Bag Ti: C.chs) {
            //Util.e("going into " + Ti.id);
            HashSet<Integer> a = Ti.spanns(C, new HashSet<Integer>());
            if(a.size() > A.size()) {
                A = a;
                TA = Ti;
            }
        }
        HashSet<Integer> B = new HashSet<>();
        for(Node nod: G.nodes) if(!A.contains(nod.id)) B.add(nod.id);
        Util.e(A.size() + " " + B.size());
        Point lbox = new Point();
        lbox.vs = new int[cs];
        for(int i = 0; i<cs; i++) lbox.vs[i] = Integer.MIN_VALUE;
        if (B.size() > 0) for(int i = 0; i<cs; i++) {
            Point[] pts = new Point[B.size()];
            int cur = 0;
            for(int b: B) {
                Point p = new Point();
                p.vs = new int[cs];
                for(int j = 0; j<cs; j++) {
                    p.vs[j] = dists[i][b] - dists[j][b];
                }
                p.v = dists[i][b];
                p.id = b;
                pts[cur++] = p;
            }
            RangeTree r = RangeTree.make_range_tree(pts, 0, true);
            int max2 = 0;
            for(int a: A) {
                Point rbox = new Point();
                rbox.vs = new int[cs];
                for(int j = 0; j<cs; j++) {
                    rbox.vs[j] = dists[j][a] - dists[i][a] - ((j < i)?1:0);
                }
                int[] q = r.max(lbox, rbox);
                int dd = q[0] + dists[i][a];
                max2 = Math.max(max2, dists[i][a] + q[0]);
            }
            max = Math.max(max, max2);
            //query each node in A.
            // Make sure we query a nonempty bags in the TD? HMMM
        }
        for(Node nod: Cn) B.add(nod.id);
        HashMap<Integer, Integer> Amap = new HashMap<>();
        HashMap<Integer, Integer> Bmap = new HashMap<>();
        tmp = 0;
        for(int i: A) Amap.put(i, tmp++);
        tmp = 0;
        for(int i: B) Bmap.put(i, tmp++);
        TA.chs.remove(C);
        C.chs.remove(TA);
        TA.replace(null, Amap);
        C.replace(null, Bmap);
        Graph GA = G.filter(Amap);
        Graph GB = G.filter(Bmap);
        max = Math.max(max, A(GA, new TreeDecomposition(TA), max));
        max = Math.max(max, A(GB, new TreeDecomposition(C), max));
        return max;

        // Build rangetree
        // query rangeTrees
        // Split G and T. (DFS in T, to find nodes, then do some 
        // remapping and rebuilng of the graph..)
        // Gonna be a pain in the ass to redo the TreeDecomposition though.
    }
    public static void main(String[] args) throws Exception{
        (new TwD()).solve(args);
    }
}
