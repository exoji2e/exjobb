Breif description of content
--------------
Compile everything with `javac *.java`

`gen.py` generates graphs that has a tree decomposition of width 2, by first generating a tree.

`Diameter.java` solves diameter in time O(n^3) with floyd warshall

`TreeDecomposition.java` holds a TreeDecomposition, and nicifies it.

`Graph.java` Just holds the data classes for Node, Edge and Graph.

`TwD.java` Will solve diameter with treewidth.

`RangeTree.java` Builds a rangetree, a datastructure over d-dimentional points.

I plan to make some unittests for each java-file, naming the tests for `X.java` `TestX.java`.
So far there exists a `TestRangeTree.java` that queiries a random set of points with random boxes,
and makes sure the answer is the same as testing each point agains the box.


