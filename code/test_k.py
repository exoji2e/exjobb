import sys, glob, random
from subprocess import Popen, PIPE, call
from test_util import *
from collections import *
seed = sys.argv[1]
def nxt(seed):
    random.seed(seed)
    return random.randint(0, 1000)

def test(n, maxk, seed):
    d = "test_k_" + seed
    call(["mkdir", "-p", d])
    for p in range(3, maxk+1):
        call(["python2", "gen.py", "-n", str(n), "-k", str(p), "-d", d, "-s", seed])
    W = []
    D = []
    for f in sorted(glob.glob(d+"/*.gr"), key=lambda x: int(x.split('/')[1].split('_')[1])):
        print(f)
        k = int(f.split('/')[1].split('_')[1]) - 1
        v1, t1 = run(['java', '-Xmx8G', 'WienerIndex', f])
        v2, t2 = run(['java', '-Xmx8G', 'NaiveWI', f])
        if v2 == '':
            t2 = 'inf'
        if v1 == '':
            t1 = 'inf'
        if v1 != v2 and v1 != '' and v2 != '':
            print('error in file {}, got {} and {}'.format(f, v1, v2))
        W.append((k, t1))
        D.append((k, t2))
    call(["rm", "-r", d])
    return W, D

def append(W, Wt):
    for k, t in Wt:
        try:
            W[k] += t
        except:
            W[k] = 'inf'


W = Counter()
D = Counter()
n, maxk, runs = 10000, 6, 10
for _ in range(runs):
    seed = str(nxt(seed))
    Wt, Dt = test(n, maxk, seed)
    append(W, Wt)
    append(D, Dt)

def genOut(W, runs):
    Wout = []
    for k, v in sorted(W.items()):
        if v == 'inf':
            Wout.append((k, v))
        else:
            Wout.append((k, v/runs))
    return Wout


Wout = genOut(W, runs)
Dout = genOut(D, runs)

call(["mkdir", "-p", "out"])
with open('out/Wn{}.csv'.format(n), 'w') as f:
    f.write(csvf(Wout))
with open('out/Dn{}.csv'.format(n), 'w') as f:
    f.write(csvf(Dout))
