import sys, glob, random
from subprocess import Popen, PIPE, call
from test_util import *
from collections import Counter
seed = sys.argv[1]
def nxt(seed):
    random.seed(seed)
    return random.randint(0, 1000)

WI = Counter()
Naive = Counter()
runs, k, maxn, minn = 10, 4, 16, 14
for _ in range(runs):
    seed = str(nxt(seed))
    d = "test_n_" + seed
    call(["mkdir", "-p", d])
    for p in range(minn, maxn):
        call(["python2", "gen.py", "-n", str(1<<p), "-k", str(k+1), "-d", d, "-s", seed])

    for f in sorted(glob.glob(d+"/*.gr"), key=lambda x: int(x.split('/')[1].split('_')[0])):
        print(f)
        n = int(f.split('/')[1].split('_')[0])
        v1, t1 = run(['java', '-Xmx8G', 'WienerIndex', f])
        v2, t2 = run(['java', '-Xmx8G', 'NaiveWI', f])
        if v2 == '':
            t2 = 'inf'
        if v1 == '':
            t1 = 'inf'
        if v1 != v2 and v1 != '' and v2 != '':
            print('error in file {}, got {} and {}'.format(f, v1, v2))
        

        if WI[n] != 'inf' and t1 != 'inf':
            WI[n] += t1
        else:
            WI[n] = 'inf'
        if Naive[n] != 'inf' and t2 != 'inf':
            Naive[n] += t2
        else:
            Naive[n] = 'inf'

    call(["rm", "-r", d])
WIout = []
Naiveout = []
for n, v in sorted(WI.items()):
    if v == 'inf':
        WIout.append((n, v))
    else:
        WIout.append((n, v/runs))

for n, v in sorted(Naive.items()):
    if v == 'inf':
        Naiveout.append((n, v))
    else:
        Naiveout.append((n, v/runs))

call(["mkdir", "-p", "out"])
with open('out/Wk{}_{}.csv'.format(k, maxn), 'w') as f:
    f.write(csvf(WIout))
with open('out/Dk{}_{}.csv'.format(k, maxn), 'w') as f:
    f.write(csvf(Naiveout))
