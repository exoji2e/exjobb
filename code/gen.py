import argparse
import sys
import random
from random import randint as ri
def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', type=int, help="size of graph")
    parser.add_argument('-k', type=int, help="largest clique")
    parser.add_argument('-r', type=bool, default=0, help="should the clique size-s vary?")
    parser.add_argument('-s', type=int, default=0, help="set seed")
    parser.add_argument('-w', type=int, default=1, help="max_weight")
    parser.add_argument('-d', default='', help="directory")
    return parser.parse_args()
args = get_args()
sz = args.n
k = args.k
r = args.r
seed = ri(0, 10000) if not args.s else args.s

random.seed(seed)

bag_node = [[i for i in range(k)]]
real_bag_map = [0]
bag_edgs = []
edgs = [(i, j) for i in range(k) for j in range(k) if i<j]
used = k
while used < sz:
    amount = ri(1 if r else min(sz-used, k), min(sz-used, k))
    real_bag_map.append(len(bag_node))
    bag_node.append([i+used for i in range(amount)])
    for i in range(amount):
        for j in range(i+1, amount):
            edgs.append((i + used, j + used))
    j = real_bag_map[ri(0, len(real_bag_map) - 2)]
    jj = bag_node[j][ri(0, len(bag_node[j]) - 1)]
    ii = used + ri(0, amount-1)
    edgs.append((jj, ii))
    bag_node.append([jj, ii])
    bag_edgs.append((j, len(bag_node) - 1))
    bag_edgs.append((len(bag_node) - 2, len(bag_node) - 1))
    used += amount

fname = '{}_{}_{}.'.format(used, k, seed)
path = (args.d + '/' if args.d else '' ) + fname

with open(path + 'td', 'w') as f:
    f.write('s td {} {} {}\n'.format(len(bag_node), k, used))
    for i, b in enumerate(bag_node):
        f.write('b {} {}\n'.format(i, ' '.join(map(str, b))))
    f.write('\n'.join(map(lambda (u,v): '{} {}'.format(u, v), bag_edgs)) + '\n')

with open(path + 'gr', 'w') as f:
    f.write('{} {}\n'.format(used, len(edgs)))
    f.write('\n'.join(map(lambda (u,v): '{} {} {}'.format(u, v, ri(1, args.w)), edgs)) + '\n')


