import java.util.*;
import java.io.*;
public class Diameter_BFS {
    public static void main(String[] args) {
        Graph g = Graph.build(args[0], args.length > 1);
        System.out.println(g.diameter_BFS());
    }
}
