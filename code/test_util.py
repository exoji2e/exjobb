from subprocess import Popen, PIPE, call
def gettime(err):
    time = ' '.join(err.split('\n')[-3:]).split()
    for v in time:
        if 'elapsed' in v:
            t = v[:-7]
            m, s = map(float, t.split(':'))
            return m*60 + s
            

def run(v):
    proc = Popen(['time'] + v, stdout=PIPE, stderr=PIPE)
    output, err = proc.communicate()
    return [output.strip(), gettime(err)]

def csvf(out):
    r = ['size,time']
    for s, t in out:
        r.append('{},{}'.format(s, t))
    return '\n'.join(r)

