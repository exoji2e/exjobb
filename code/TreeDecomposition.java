import java.util.*;
import java.io.*;
public class TreeDecomposition {
    int k;
    Bag[] bags;
    public static TreeDecomposition build(String tdpath, boolean oneIndex) {
        BufferedReader td = null;
        try{
            td = new BufferedReader(new FileReader(tdpath));
        }catch(Exception e) {
            e.printStackTrace();
        }
        String line = Util.nxt(td);
        String[] a = line.split(" ");
        int bsz = Util.toInt(a[2]);
        int w = Util.toInt(a[3]);
        int nn = Util.toInt(a[4]);
        int sub = oneIndex? 1:0;
        Bag[] bags = new Bag[bsz];
        for(int i = 0; i<bsz; i++) {
            bags[i] = new Bag();
            a = Util.nxt(td).split(" ");
            for(int j = 2; j< a.length; j++){
                bags[i].nodes.add(Integer.parseInt(a[j]) - sub);
            }
        }

        for(int i = 1; i<bsz; i++) {
            int[] xx = Util.toInts(Util.nxt(td));
            bags[xx[0] - sub].chs.add(bags[xx[1] - sub]);
            bags[xx[1] - sub].chs.add(bags[xx[0] - sub]);
        }
        for(Bag b: bags)
            Bag.sz=Math.max(Bag.sz, b.nodes.size());
        // Creation niceifies T
        return new TreeDecomposition(bags, true);
    }
    public TreeDecomposition(Bag[] bags) {
        this(bags, true);
    }
    public TreeDecomposition(Bag[] bags, boolean nicify) {
        if(nicify) {
            HashSet<Bag> bset = new HashSet<>();
            HashSet<Bag> parents = new HashSet<>();
            for(Bag b: bags){
                b.nicify(bset); break;
            }
            this.bags = new Bag[bset.size()];
            int i = 0;
            for(Bag b: bset)
                this.bags[i++] = b;
        } else{
            this.bags = bags;
        }
        k=Bag.sz;
    }
    public TreeDecomposition(Bag bag) {
        HashSet<Bag> bgs = new HashSet<>();
        bag.reachable(null, bgs);
        bags = new Bag[bgs.size()];
        int i = 0;
        for(Bag b: bgs) bags[i++] = b;
    }
    public Bag centroid(int t) {
        //Let's try this for now...
        //Plan is to create a dfs.
        bags[0].height(null);
        for(Bag b: bags)
            return b.centroid(null, bags[0].height);
        return null;
    }
    public String toString() {
        StringBuilder sb = new StringBuilder();
        int tmp = 0;
        for(Bag b: bags) {
            b.id = tmp++;
            sb.append("B: ");
            for(int i: b.nodes) sb.append(i).append(' ');
            sb.append('\n');
        }
        for(Bag b: bags) {
            for(Bag b2: b.chs) {
                sb.append(b.id).append(' ').append(b2.id).append('\n');
            }
        }
        return sb.toString();
    }
}
class Bag {
    int type;
    int id;
    int height;
    static int sz = 0;
    public Bag() {}
    HashSet<Integer> nodes = new HashSet<>();
    HashSet<Bag> chs = new HashSet<>();
    Bag first(HashSet<Bag> ch) {
        for(Bag b: ch) 
            return b;
        return null;
    }
    int height(Bag p) {
        height = 1;
        for(Bag b: chs) if(b!=p) {
            height += b.height(this);
        }
        return height;
    }
    Bag centroid(Bag p, int toth) {
        int maxh = 0;
        Bag maxb = null;
        for(Bag b: chs) {
            if(b == p) b.height = toth - height;
            if(b.height > maxh) {
                maxb = b;
                maxh = b.height;
            }
        }
        if(maxh*3 > toth && maxh*3 < toth*2 || maxb == null || maxb == p) {
            return this;
        }
        return maxb.centroid(this, toth);
    }
    boolean old = false;
    public Bag nicify(HashSet<Bag> bgs) {
        this.old = true;
        //System.out.println(nodes);
        Bag b = new Bag();
        b.nodes = Util.copy(nodes);
        bgs.add(b);
        if(chs.size() == 0) return b;
        if(chs.size() == 1) {
            Bag last = first(chs);
            last.chs.remove(this);
            Bag c = new Bag();
            c.nodes = Util.copy(nodes);
            c.chs.add(last);
            for(Integer n: nodes) {
                if(!last.nodes.contains(n)) {
                    c.nodes.remove(n);
                    c = c.nicify(bgs);
                    add_edge(b, c);
                    return b;
                }
            }
            for(Integer n: last.nodes) {
                if(!nodes.contains(n)) {
                    c.nodes.add(n);
                    c = c.nicify(bgs);
                    add_edge(b, c);
                    return b;
                }
            }
            bgs.remove(b);
            return last.nicify(bgs);
        } else {
            Bag c1 = new Bag();
            Bag c2 = new Bag();
            c1.nodes = Util.copy(nodes);
            c2.nodes = Util.copy(nodes);
            Bag toc1 = first(chs);
            c1.chs.add(toc1);
            for(Bag cc: chs) {
                cc.chs.remove(this);
                if(cc != toc1) c2.chs.add(cc);
            }
            c1 = c1.nicify(bgs);
            c2 = c2.nicify(bgs);
            add_edge(b, c1);
            add_edge(b, c2);
        }
        return b;

    }
    static void add_edge(Bag a, Bag b) {
        a.chs.add(b);
        b.chs.add(a);
    }
    public HashSet<Integer> spanns(Bag p, HashSet<Integer> v) {
        for(Integer n: nodes) v.add(n);
        for(Bag b: chs) if(b != p && b!=null) b.spanns(this, v);
        return v;
    }

    public HashSet<Bag> reachable(Bag p, HashSet<Bag> v) {
        v.add(this);
        for(Bag b: chs) if(b != p && b!=null) b.reachable(this, v);
        return v;
    }
    public void replace(Bag p, HashMap<Integer, Integer> map) {
        HashSet<Integer> repl = new HashSet<>();
        for(int i : nodes) {
            if(!map.containsKey(i)) throw new IllegalArgumentException();
            repl.add(map.get(i));
        }
        nodes = repl;
        for(Bag b: chs) if(b != p && b != null) b.replace(this, map);
    }
}
