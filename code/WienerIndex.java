import java.util.*;
import java.io.*;
public class WienerIndex {
    void solve(String[] args) throws Exception {
        boolean oneIndex = args.length > 1;
        String grpath = args[0];
        // Parse data
        Graph G = Graph.build(grpath, oneIndex);

        String tdpath = grpath.substring(0, grpath.length() - 3) + ".td";
        TreeDecomposition T = TreeDecomposition.build(tdpath, oneIndex);
        Util.e(String.format("%d bags in td", T.bags.length));

        System.out.println(A(G, T));

    }
    public long A(Graph G, TreeDecomposition T) {
        int n = G.nodes.length, k=T.k;
        if(k > 30 || Math.max(1<<k, 10) > n || T.bags.length < 3){
            return G.wienerind();
        }
        if(k > 10){
            System.out.println(n);
            System.out.println(k);
            throw new IllegalArgumentException(); 
        }
        // Find centroid C of T
        Bag C = T.centroid(n/2);
        int cs = C.nodes.size();
        int[][] dists = new int[cs][n];
        int tmp = 0;
        Node[] Cn = new Node[cs];
        // dijkstra from each node in C
        long wi = 0;
        long rm = 0; //Will be doublec counted
        for(Integer nod: C.nodes) {
            Node node = G.nodes[nod];
            G.dijkstra(node, dists[tmp]);
            Cn[tmp++] = node;
        }
        tmp = 0;
        for(Integer c_i: C.nodes) {
            for(Integer c_j: C.nodes) {
                if(c_j != c_i) 
                    G.upd_edge(c_i, c_j, dists[tmp][c_j]);
                
                if(c_j < c_i)
                    rm += dists[tmp][c_j];
            }
            tmp++;
        }

        //Not the diameter, just making sure we calculate the canidates right.

        Util.e(String.format("GraphSize %d", n));

        Bag TA = null;
        HashSet<Integer> A = new HashSet<>();
        for(Bag Ti: C.chs) {
            HashSet<Integer> a = Ti.spanns(C, new HashSet<Integer>());
            if(a.size() > A.size()) {
                A = a;
                TA = Ti;
            }
        }
        for(int i: C.nodes) A.add(i);
        HashSet<Integer> B = new HashSet<>();
        for(Node nod: G.nodes) if(!A.contains(nod.id) || C.nodes.contains(nod.id)) B.add(nod.id);
        Point lbox = new Point();
        lbox.vs = new int[cs];
        for(int i = 0; i<cs; i++) lbox.vs[i] = Integer.MIN_VALUE/2;
        if (B.size() - C.nodes.size() > 0) for(int i = 0; i<cs; i++) {
            Point[] pts = new Point[B.size() - C.nodes.size()];
            int cur = 0;
            for(int b: B) if(!C.nodes.contains(b)){
                Point p = new Point();
                p.vs = new int[cs];
                for(int j = 0; j<cs; j++) {
                    p.vs[j] = dists[i][b] - dists[j][b];
                }
                p.v = dists[i][b];
                p.id = b;
                pts[cur++] = p;
            }
            RangeTree2 r = RangeTree2.make_range_tree(pts, 0, true);
            int max2 = 0;
            for(int a: A) if(!C.nodes.contains(a)){
                Point rbox = new Point();
                rbox.vs = new int[cs];
                for(int j = 0; j<cs; j++) {
                    rbox.vs[j] = dists[j][a] - dists[i][a] - ((j < i)?1:0);
                }
                long[] q = r.sum(lbox, rbox);
                wi += q[0]*dists[i][a] + q[1];
            }
        }
        HashMap<Integer, Integer> Amap = new HashMap<>();
        HashMap<Integer, Integer> Bmap = new HashMap<>();
        tmp = 0;
        for(int i: A) Amap.put(i, tmp++);
        tmp = 0;
        for(int i: B) Bmap.put(i, tmp++);
        Bag CC = new Bag();
        for(int i: C.nodes) CC.nodes.add(i);
        CC.chs.add(TA); TA.chs.add(CC);
        C.chs.remove(TA); TA.chs.remove(C);
        CC.replace(null, Amap);
        C.replace(null, Bmap);
        Graph GA = G.filter(Amap);
        Graph GB = G.filter(Bmap);
        wi -= rm; //Counted inside both GA and GB.
        long wiA = A(GA, new TreeDecomposition(CC));
        long wiB = A(GB, new TreeDecomposition(C));
        wi += wiA + wiB;
        return wi;
    }
    public static void main(String[] args) throws Exception{
        (new WienerIndex()).solve(args);
    }
}
