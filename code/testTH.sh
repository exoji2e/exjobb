#!/bin/bash
for f in TH/*.gr; do
    echo $f
    java TwD $f 1 2> /dev/null > out
    java Diameter_BFS $f 1 > out2
    diff out out2
    rm out out2
done
