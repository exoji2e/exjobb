import java.util.*;
class Point {
    int[] vs;
    int v;
    int id;
}
class PointC implements Comparator<Point> {
    int sortd;
    public PointC(int d){sortd = d;}
    public int compare(Point p, Point q) {
        int diff = p.vs[sortd] - q.vs[sortd];
        if(diff != 0) return diff;
        return p.id - q.id;
    }
}
class RangeTree {
    Point[] ps;
    int d, max;
    int id;
    RangeTree nextD, left, right;

    public void log(StringBuilder sb) {
        for(Point p : ps)
            sb.append(p.id).append(' ');
        sb.append('\n');
        if(left != null) left.log(sb);
        if(right != null) right.log(sb);
        if(nextD != null) nextD.log(sb);
    }

    static RangeTree make_range_tree(Point[] ps, int d, boolean sort) {
        ps = Arrays.copyOf(ps, ps.length);
        if(sort) {
            Arrays.sort(ps, new PointC(d));
        }
        RangeTree here = new RangeTree();
        here.ps = ps;
        here.d = d;
        if(ps.length > 1){
            Point[] lp = Arrays.copyOfRange(ps, 0, ps.length/2);
            Point[] rp = Arrays.copyOfRange(ps, ps.length/2, ps.length);
            here.left = make_range_tree(lp, d, false);
            here.right = make_range_tree(rp, d, false);
        }
        if(!here.last())
            here.nextD = make_range_tree(ps, d+1, true);
        else {
            int max = ps[0].v;
            int id = ps[0].id;
            if(here.left != null && max < here.left.max) {
                max = here.left.max;
                id = here.left.id;
            }
            if (here.right != null && max < here.right.max) {
                max = here.right.max;
                id = here.right.id;
            }
            here.max = max;
            here.id = id;
        }
        return here;
    }
    int[] max(Point lbox, Point rbox) {
        int lps = ps[0].vs[d], rps = ps[ps.length -1].vs[d];
        int l = lbox.vs[d], r = rbox.vs[d];
        if (l <= lps && r >= rps) {
            return last() ? new int[]{max, id} : nextD.max(lbox, rbox);
        }
        if (l > rps || r < lps) return new int[]{Integer.MIN_VALUE/2, -1};
        int[] lm = left.max(lbox, rbox), rm = right.max(lbox, rbox);
        if(lm[0] > rm[0]) return lm;
        else return rm;
    }
    boolean last() {
        return (d == ps[0].vs.length - 1);
    }
}
