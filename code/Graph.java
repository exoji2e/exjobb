import java.util.*;
import java.io.*;
public class Graph {
    Node[] nodes;
    public Graph(int sz) {
        nodes = new Node[sz];
        for(int i = 0; i<sz; i++)
            nodes[i] = new Node(i);
    }
    public static Graph build(String grpath) {
        return build(grpath, false);
    }
    public static Graph build(String grpath, boolean oneIndex) {
        BufferedReader gr = null;
        try{
            gr = new BufferedReader(new FileReader(grpath));
        }catch(Exception e) {
            e.printStackTrace();
        }
        String s = Util.nxt(gr);
        String[] a = s.split(" ");
        int n = Util.toInt(a[a.length - 2]);
        int m = Util.toInt(a[a.length - 1]);
        Graph G = new Graph(n);
        int sub = oneIndex? 1 : 0;
        for(int i = 0; i<m; i++) {
            int[] xx = Util.toInts(Util.nxt(gr));
            int v = xx.length == 2? 1 : xx[2];
            G.add_edge(xx[0]-sub, xx[1]-sub, v);
            G.add_edge(xx[1]-sub, xx[0]-sub, v);
        }
        int[] v = new int[n];
        G.dijkstra(G.nodes[0], v);
        for(int i : v){
            if(i == -1) Util.fail();
        }
        return G;
    }
    int[] toInts(String s) {
        String[] a = s.split(" ");
        int[] o = new int[a.length];
        for(int i = 0; i<a.length; i++) o[i] = toInt(a[i]);
        return o;
    }
    int toInt(String s) {return Integer.parseInt(s);}

    void add_edge(int i, int j, int c) {
        Edge e1 = new Edge(nodes[j], c);
        nodes[i].edgs.add(e1);
    }
    void upd_edge(int i, int j, int c) {
        boolean done = false;
        for(Edge e: nodes[i].edgs) {
            if(e.to.id != j) continue;
            e.c = c;
            done=true;
        }
        if(!done){
            add_edge(i, j, c);
        }
        
    }
    private class Pair implements Comparable<Pair> {
        int v;
        int id;
        public Pair(int v, int nod){this.v=v;this.id=nod;}
        public int compareTo(Pair p){
            if(v != p.v) return v - p.v;
            return id - p.id;
        }
    }
    public void dijkstra(Node start, int[] dists) {
        int INF = Integer.MAX_VALUE;
        Arrays.fill(dists, INF);
        dists[start.id] = 0;
        Pair here = new Pair(0, start.id);
        PriorityQueue<Pair> set = new PriorityQueue<>();
        set.add(here);
        while(!set.isEmpty()) {
            Pair p = set.poll();
            if(dists[p.id] < p.v) continue;
            for(Edge e: nodes[p.id].edgs) {
                Node n = e.to;
                if(p.v + e.c < dists[n.id]) {
                    dists[n.id] = p.v + e.c;
                    set.add(new Pair(dists[n.id], n.id));
                }
            }
        }
        for(int i = 0; i<dists.length; i++) if(dists[i] == INF) dists[i] = -1;
    }
    long wienerind() {
        int n = nodes.length;
        int[][] dist = new int[n][n];
        long sum = 0;
        for(Node u: nodes) {
            dijkstra(u, dist[u.id]);
            for(int i = 0; i<n; i++) 
                if(i!=u.id)
                    sum += dist[u.id][i];
        }
        return sum/2;
    }
    int diameter_dijk() {
        int n = nodes.length;
        int[][] dist = new int[n][n];
        int max = 0;
        for(Node u: nodes) {
            dijkstra(u, dist[u.id]);
            for(int i = 0; i<n; i++) max = Math.max(max, dist[u.id][i]);
        }
        return max;
    }
    int diameter_FW() {
        int n = nodes.length;
        int[][] dist = new int[n][n];
        for(Node u: nodes) {
            dist[u.id][u.id] = 0;
            for(Edge e: u.edgs) {
                dist[u.id][e.to.id] = e.c;
            }
        }
        for (int k = 0; k<n; k++) for(int i = 0; i<n; i++) for(int j = 0; j<n; j++) {
            if (dist[i][j] > dist[i][k] + dist[k][j])
                dist[i][j] = dist[i][k] + dist[k][j];
        } 
        int max = 0;
        for(int i = 0; i<n; i++) {
            for(int j = 0; j<n; j++) {
                max = Math.max(max, dist[i][j]);
            }
        }
        return max;
    }
    int diameter_BFS() {
        int n = nodes.length;
        int max = 0;
        for(int i = 0; i<n; i++) {
            boolean[] vis = new boolean[n];
            LinkedList<Integer> q = new LinkedList<>();
            q.add(i);
            vis[i] = true;
            int cnt = 0;
            while(!q.isEmpty()) {
                LinkedList<Integer> q2 = new LinkedList<>();
                for(int u: q) {
                    for(Edge e: nodes[u].edgs) {
                        int v = e.to.id;
                        if(!vis[v]) {
                            q2.add(v);
                            vis[v] = true;
                        }
                    }
                }
                q = q2;
                cnt += 1;
            }
            max = Math.max(max, cnt - 1);
        }
        return max;
    }
    Graph filter(HashMap<Integer, Integer> map) {
        Graph nxt = new Graph(map.size());
        for(int i: map.keySet()) {
            int ni = map.get(i);
            Node prev = nodes[i];
            for(Edge e: prev.edgs) {
                if(map.containsKey(e.to.id)) 
                    nxt.add_edge(ni, map.get(e.to.id), e.c); 
            }
            
        }
        return nxt;
    }
}
class Node {
    LinkedList<Edge> edgs;
    int id;
    public Node(int id) {
        this.id = id; edgs = new LinkedList<>();
    }
    public String toString(){return "" + id;}
}
class Edge {
    Node to;
    int c;
    public Edge(Node to, int c) {
        this.to = to; this.c = c;
    }
}
