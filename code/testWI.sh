#!/bin/bash
for f in TH/*.gr; do
    echo $f
    java WienerIndex $f 1 2> /dev/null > out1
    java NaiveWI $f 1 > out2
    diff out1 out2
    rm out1 out2
done
