import java.util.*;
class RangeTree2 {
    Point[] ps;
    int d;
    long sum, no;
    int id;
    RangeTree2 nextD, left, right;

    public void log(StringBuilder sb) {
        for(Point p : ps)
            sb.append(p.id).append(' ');
        sb.append('\n');
        if(left != null) left.log(sb);
        if(right != null) right.log(sb);
        if(nextD != null) nextD.log(sb);
    }

    static RangeTree2 make_range_tree(Point[] ps, int d, boolean sort) {
        ps = Arrays.copyOf(ps, ps.length);
        if(sort) {
            Arrays.sort(ps, new PointC(d));
        }
        RangeTree2 here = new RangeTree2();
        here.ps = ps;
        here.d = d;
        if(ps.length > 1){
            Point[] lp = Arrays.copyOfRange(ps, 0, ps.length/2);
            Point[] rp = Arrays.copyOfRange(ps, ps.length/2, ps.length);
            here.left = make_range_tree(lp, d, false);
            here.right = make_range_tree(rp, d, false);
        }
        if(!here.last())
            here.nextD = make_range_tree(ps, d+1, true);
        else {
            int no = 0;
            int sum = 0;
            int id = ps[0].id;
            if(here.left != null) {
                sum += here.left.sum;
                no += here.left.no;
            }
            if (here.right != null){
                sum += here.right.sum;
                no += here.right.no;
            }
            if(no == 0){
                here.no = 1;
                here.sum = ps[0].v;
            } else{
                here.no = no;
                here.sum = sum;
            }
        }
        return here;
    }
    long[] sum(Point lbox, Point rbox) {
        int lps = ps[0].vs[d], rps = ps[ps.length -1].vs[d];
        int l = lbox.vs[d], r = rbox.vs[d];
        if (l <= lps && r >= rps) {
            return last() ? new long[]{no, sum} : nextD.sum(lbox, rbox);
        }
        if (l > rps || r < lps) return new long[]{0, 0};
        long[] lm = left.sum(lbox, rbox), rm = right.sum(lbox, rbox);
        lm[0] += rm[0];
        lm[1] += rm[1];
        return lm;
    }
    boolean last() {
        return (d == ps[0].vs.length - 1);
    }
}
