import sys
import random
from random import randint as ri
if len(sys.argv) > 1:
    sz = int(sys.argv[1])
else:
    sz = 1000
if len(sys.argv) > 2:
    seed = int(sys.argv[-1])
else:
    seed = 1337

random.seed(seed)

tree = [[] for _ in range(sz)]

for i in range(sz - 1, 0, -1):
    j = random.randint(0, i-1)
    tree[j].append(i)

bag_node = [[] for _ in range(2*sz - 1)]
bag_edgs = []
edgs = []

cnt = sz

for i, ch in enumerate(tree):
    bag_node[i] = [3*i + u for u in range(3)]
    edgs.append((3*i, 3*i + 1))
    edgs.append((3*i, 3*i + 2))
    edgs.append((3*i + 1, 3*i + 2))
    for c in ch:
        a, b = 3*i + ri(0, 2), 3*c + ri(0, 2)
        bag_node[cnt] = [a, b]
        bag_edgs.append((i, cnt))
        bag_edgs.append((cnt, c))
        edgs.append((a, b))
        cnt += 1

with open('{}_{}.td'.format(sz, seed), 'w') as f:
    f.write('s td {} {} {}\n'.format(len(bag_node), 2, 3*sz))
    for i, b in enumerate(bag_node):
        f.write('b {} {}\n'.format(i, ' '.join(map(str, b))))
    f.write('\n'.join(map(lambda (u,v): '{} {}'.format(u, v), bag_edgs)) + '\n')

with open('{}_{}.gr'.format(sz, seed), 'w') as f:
    f.write('{} {}\n'.format(sz*3, sz*4 - 1))
    f.write('\n'.join(map(lambda (u,v): '{} {}'.format(u, v), edgs)) + '\n')


