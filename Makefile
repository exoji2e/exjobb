latexfile = msc
bibfile = msc

$(latexfile).pdf: $(latexfile).tex $(bibfile).bib
	while (pdflatex $(latexfile) ; \
	grep -q "Rerun to get cross" $(latexfile).log ) do true ; \
	done ; \
	bibtex $(latexfile) ; \
	while (pdflatex $(latexfile) ; \
	grep -q "Rerun to get cross" $(latexfile).log ) do true ; \
	done

all: $(latexfile).pdf

clean:
	rm *.log
	rm *.aux
	rm *.out
	rm *.class

